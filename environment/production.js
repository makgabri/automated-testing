const { Before } = require('cucumber');

Before(function() {
    this.environment = 'production';
    
    // API Variables
    this.web_url = 'https://app.streamloan.io';
});