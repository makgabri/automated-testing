const { Before } = require('cucumber');

Before(function() {
    this.environment = 'staging';
    
    // API Variables
    this.web_url = 'https://gsapp.streamloan.io';
});