const { Before } = require('cucumber');

Before(function() {
    this.environment = 'development'
    
    // API Variables
    this.web_url = 'https://gdapp.streamloan.io';
});