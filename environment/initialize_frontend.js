let { Builder, By } = require('selenium-webdriver');
let fs = require('fs');



/** TEMPLATE TO AUTOMATE IMAGE TAKING
    (async function example() {
    let driver = await new Builder()
      .forBrowser('chrome')
      .build();

    await driver.manage().setTimeouts( { implicit: 3000 } );
    await driver.get('https://master-webapp.streamloan.io/login');
    let webElement = driver.findElement(By.id("app"));
    await driver.takeScreenshot().then(function(data) {
        writeScreenshot(data, 'login.png');
        driver.quit();
    });
    }());
*/



/** HELPER FUNCTION
 * Save screenshot from driver into an image
 * 
 * @param {string} data base64 encoded string of screen shot from driver
 * @param {string} name name to save image
 */
function writeScreenshot(data, name) {
    name = name || 'no_name.png';
    var screenshotPath = './features/frontend/images/';
    fs.writeFileSync(screenshotPath + name, data, 'base64');
};



/** ACTUAL AUTOMATE PICTURE TAKING BELOW */
// Save Login Page
(async function example() {
    let driver = await new Builder()
      .forBrowser('chrome')
      .build();

    await driver.manage().setTimeouts( { implicit: 3000 } );
    await driver.get('https://master-webapp.streamloan.io/login');
    let webElement = driver.findElement(By.id("app"));
    await driver.takeScreenshot().then(function(data) {
        writeScreenshot(data, 'login.png');
        driver.quit();
    });
}());
  
