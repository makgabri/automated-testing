# Cucumber Testing with Xray reporting

## Introduction
The goal of this testing repository is to execute unit tests described from a user's point of view and report the results to the xray board.

## How it Works
1. Define an aspect of the app to test.
    * For example: API endpoints
2. Using [Gherkin Syntax](https://cucumber.io/docs/gherkin/), describe how a user would expect to utilize this aspect.
    * For example: As a loaner, when I get loan folders, then schema should be loan folder
3. Define tests on streamloan xray board as a cucumber test
4. Use cucumber as testing method and place the test from #2 into the cucumber space
5. Define cucumber steps in features/[aspect] folder
6. Run index.js with the desired command line options(upload/no-upload)

## Testing Aspects
1. API testing
    * Completed public endpoints
    * Completed most private endpoints
    * Uncompleted legacy endpoints
    * Currently tests response code and response schema defined in the corresponding step definitions
2. Front end testing
    * A basic example is ready and can be extended easily.
        * Initialize the screenshot images in initialize_frontend.js. This is seperately run from the automated test as this requires some manual work to make sure the images are to be correct
        *
    * Working flow is as follows:
        1. First, personally examine the webpages expected output. For example, take a look at master-webapp.streamloan.io and make sure that it is meant to look like that.
        2. In environment/initialize_frontend.js copy and paste the template and change the url to the one you want to test. Selenium offers options to execute actions such as click on element with id xxx.(More can be found on Selenium link below)
        3. Run the following in a command line interface:
        ```
        npm run init_frontend
        ```
        4. Now the images are updated and can be found in /features/frontend/images
        5. Run the test as usual. An example would be: 
        ```
        node index.js -k STRM-118 -g frontend -e staging
        ```
        6. Results will be shown. The screenshot taken from the automated test will be written in /features/frontend/temp with the same image name. If the two images are different, then the script will generate an image that highlights the differences in the two images and will be stored in /features/frontend/diffs with the same image name
3. Back end testing

## Pointers
* Define environment variables in environment folder
* Define specific scenario variables in feature
* Use of unit test here is not that efficient. There are various reasons but the main reason being that the ultimate goal here is to produce a report of tests that prove the application is fully functional from what a user can see and use. Therefor unit testing would report excess information and do extra work than needed. Unit testing should ultimately remain in the working repository so that engineer can immediately debug and see if there are any breaks as they work.

## How to use after defining necessary steps
1. Install dependencies. From command line run:
```
npm install
```
2. Run index.js according to how you want to use it by defining the arguments. Order of arguments doesn't matter. The possible arguments are:
    1. Key
        * Alias: k
        * Required: Yes
        * Description: The key of the ticket containing the test on xray board.
    2. Group
        * Alias: g
        * Required: Yes
        * Description: The group corresponding to the testing aspect(currently only api, frontend and backend are available)
    3. Upload
        * Alias: u
        * Required: No
        * Description: Indicates whether to upload results to xray board. If key is a test execution, results will be uploaded to that ticket. Otherwise, a new test execution will be created and results will be uploaded to new test execution ticket.
    4. Environment
        * Alias: e
        * Required: Yes
        * Description: Indicates environment when testing

### Examples
Running api test from ticket 25 in development
```
node index.js -g api -k STRM-25 -e development
```
Running api test from ticket 47 in staging and uploading results
```
node index.js -g api -k STRM-47 -e staging -u
```

## Useful Information
* [Cucumber](https://cucumber.io)
* [Jira Xray](https://docs.getxray.app/display/XRAYCLOUD/About+Xray)
* [Selenium](https://www.selenium.dev/documentation/en/webdriver/)