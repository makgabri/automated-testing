/** 
  * File:   index.js 
  * 
  * Date:   June 2020
  * 
  * Summary of File: 
  * 
  *   Manages the execution of running a test.
  * 
  *   Flow of execution:
  *     1. Pre-requisite: Define tests on StreamLoan Xray Board
  *     2. Using the ticket number (STRM-##), define it in config.json or supply as an argument
  *     3. This file will then collect the cucumber test and save it into the correct spot(see folder structure)
  *     4. This file will run (./node_modules/.bin/cucumber-js) which executes the tests
  *     5. This file will then display the result OR automatically upload the results back(depends if -u argument supplied)
  * 
  *   Only one group at a time can be specified unless defined in config.json
  * 
  *   It should be noted that, if upload argument is supplied, and the ticket number is a test execution ticket, the results will
  *   automatically be uploaded back to the test execution ticket. If the ticket number is not a test execution ticket, then
  *   a test execution ticket will automatically be created and the new ticket number will be displayed.
  * 
  *   Folder structure:
  *   - index.js                            -> Main file
  *   - features                            -> Contains all features/groups/stepdefs/schemas
  *       - api                             -> each group may be different since there are different testing methods but generally it will be like api
  *           - step_definitions            -> Required for all groups to define cucumber steps
  *           - schemas                     -> Contains all json schemas
  *           - XXXXXX.feature here
  *       - backend
  *       - frontend
  *       - (other test groups)
  *   - environment
  *       - development                     -> variables for development testing
  *       - staging                         -> variables for staging testing
  *       - production                      -> variables for production testing
  *   - old_test_results                    -> after uploading, a json file containing cucumber results are saved here
  * 
  *   Usage:
  *     - node index.js                     -> executes tests defined in config.json
  *     - node index.js -u                  -> executes tests defined in config.json and upload results
  *     - node index.js -g api -k STRM-47   -> executes tests defined in ticket STRM-47
  *  
  */

const axios = require('axios');
const config = require('./config.json');
const JSZip = require('jszip');
const shell = require('shelljs');
const path = require('path');
const fs = require('fs');
const { spawn } = require('child_process');
const argv = require('yargs')
        .option('key', {
            alias: 'k',
            describe: 'run test execution on xray board with key, must also provide group',
            nargs: 1
        })
        .option('group', {
            alias: 'g',
            describe: 'run tests that belongs to a certain group, must also provide key',
            nargs: 1,
            choices: ['api', 'frontend', 'backend']
        })
        .option('upload', {
            alias: 'u',
            describe: 'upload test results to xray board',
            nargs: 0
        })
        .option('env', {
            alias: 'e',
            describe: 'run tests with environment',
            nargs: 1,
            choices: ['development', 'staging', 'production']
        })
        .usage('Usage: node $0 -k [test exec key] -g [test group] -e development -u')
        .example('node $0 -e development', 'Gets default tests defined in config and runs them')
        .example('node $0 -k STRM-36 -g api -e development -u', 'Gets tests/features from ticket number 36, runs them and uploads them')
        .alias('h', 'help')
        .argv;



// Helper functions
function get_features(group, test_list) {
    let promise = new Promise((resolve, reject) => {
        // Remove all features
        fs.readdir('./features/' + group, (err, files) => {
            if (err) throw err;
        
            for (const file of files) {
                if (file.endsWith(".feature")) {
                    fs.unlink('./features/'+ group + '/' + file, (err) => {
                        if (err) throw err;
                    });
                };
            };
        });

        // Prepare get url
        let url = 'https://xray.cloud.xpand-it.com/api/v1/export/cucumber?keys=';
        // If test_set is null, get from config, else use testset
        if (test_list) {
            test_list.forEach((key) => {
                url += (key + ';');
            });
            url = url.slice(0, -1);
        } else {
            url += config[group];
        }

        // Get saved token and use it to send request
        xray_auth_token.then((token) => {
            // Send request to get features
            axios.get(url,
                {
                    headers: {'Content-Type': 'application/json', 'Authorization': `Bearer ${token.data}`},
                    responseType: 'arraybuffer'
                })
                .then((response) => {
                    // Contents is an array so use jszip to load it
                    JSZip.loadAsync(response.data)
                    .then((zip) => {
                        zip.forEach((relativePath, zipEntry) => {
                            zip.file(relativePath).async("string").then((data) => {
                                // Write contents into a feature file in the correct group
                                fs.writeFile(`./features/${group}/${relativePath}`, data, function(err) {
                                    if (err) {
                                        console.log("[Error] ", err);
                                        process.exit(1);
                                    }

                                    console.log("[Saved] Saved file ", relativePath);
                                    resolve(true);
                                });
                            });
                        });
                    });
                })
                .catch((error) => {
                    console.log(error.response.data.error);
                    process.exit(1);
                });
        });
    });
    return promise;
}



/**
 * Main operation below
 */

// Variables for later use
let default_tp = true;
let features_to_get = [];
let key_list = null;
let xray_auth_token = axios.post('https://xray.cloud.xpand-it.com/api/v1/authenticate', {
    client_id: config["xray-client-id"],
    client_secret: config["xray-client-secret"]
}); 

// Ensure token is valid
xray_auth_token.catch((error) => {
    console.log("[Error] ", error.response.data.error);
    process.exit(1);
});

// Handle environment setup
if (!argv.e) {
    console.log("[Error] Missing environment argument")
    process.exit(1);
} else {
    fs.copyFileSync(`./environment/${argv.e}.js`, './features/environment.js');
}

// Determine whether to run default test plan or supplied tests
if (argv.k) {
    if (!argv.g) {
        console.log("[Error] Missing group argument");
        process.exit(1);
    }
    default_tp = false;
    key_list = (Array.isArray(argv.k)) ? argv.k : [argv.k];
}

if (argv.g) {
    if (Array.isArray(argv.g)) {
        console.log("[Error] Only supply one group at a time");
        process.exit(1);
    }
    if (!argv.k) {
        console.log("[Error] Missing key argument");
        process.exit(1);
    }
    default_tp = false;
    features_to_get.push(argv.g);
}

if (features_to_get.length === 0) features_to_get.push("api", "frontend", "backend");



// Get features
let promise_list = features_to_get.map((group) => {
    return get_features(group, key_list);
});

// Only proceed after features have been collected
Promise.all(promise_list)
.then(() => {
    if (argv.u) {
        console.log("[INFO] Executing tests");
        const {stdout} = shell.exec("./node_modules/.bin/cucumber-js -f json", {silent: true});
        console.log("[INFO] Done executing tests");
        xray_auth_token.then((token) => {
            console.log("[INFO] Uploading execution results");
            axios({
                method: 'POST',
                url: 'https://xray.cloud.xpand-it.com/api/v1/import/execution/cucumber',
                headers: {'Content-Type': 'application/json', 'Authorization': `Bearer ${token.data}`},
                data: stdout
            }).then((response) => {
                    console.log('[INFO] Successfully uploaded execution results. Response data:');
                    console.log(response.data);
    
                    let date = new Date();
                    fs.writeFile(`old_test_results/test_${date.toUTCString().replace(/[ :]+/g, '_')}.json`, stdout, function(err) {
                        if (err) {
                            console.log("[Error] ", err);
                            process.exit(1);
                        }
    
                        console.log(`[Saved] Saved file old_test_results/test_${date.toUTCString().replace(/[ :]+/g, '_')}.json`);
                    });
                })
                .catch((error) => {
                    console.log(error.response.data.error)
                    process.exit(1);
                });
        });
    } else {
        let pathto = ['node_modules', '.bin', 'cucumber-js'].join(path.sep);
        spawn(pathto, null, {stdio: "inherit"});
    }
});
