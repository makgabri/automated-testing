Feature: Get a survey template using id with invalid headers

	#*Method:* GET
	#
	#*Path:*  /api/private/surveytemplates
	#
	#*Headers:* application identifier, current lender position
	#
	#*Query:* access token
	#
	#*Purpose:* Get a list of all survery templates available
	#
	#*Testing:* 
	#
	## Negative: Missing current lender position
	#{code:json}{
	#    "error": {
	#        "message": "Bad Request",
	#        "code": 400,
	#        "debugMsg": "current-lender-position header is missing"
	#    }
	#}{code}
	## Negative: Invalid current lender position
	#{code:json}{
	#    "error": {
	#        "message": "Forbidden",
	#        "code": 403,
	#        "debugMsg": "3 position is forbidden"
	#    }
	#}{code}
	@TEST_STRM-66
	Scenario Outline: Get a survey template using id with invalid headers
		Given I am logged in as loaner
		And I collect a survey template id
		And I want to collect more information for that survey template id
		And the current lender position is <clp_state>
		When I send GET request
		Then the response status should be <code>
		And the response type should be JSON
		And the response schema should be an error
		And the error response message is <message>
		Examples:
		    | role      | clp_state | code  | message       |
		    | loaner    | missing   | 400   | Bad Request   |
		    | loaner    | invalid   | 403   | Forbidden     |
