/**
 * Schema Step Definitions.
 * 
 * Use this step to assert the schema of a response.
 * Steps to utilize this step:
 *  1. Define schema in json format.
 *  2. Place schem file in schemas directory.
 *  3. Retrieve the schema information by requiring below in the "Retrieving the schema information" section.
 *  4. In the schemas object, define a name to represent this schema.
 *      (e.g error schema -> error)
 *  5. Use the cucumber step along with data tables.
 *
 * Data table format is described below in the step definition.
 * 
 * 
 * @see JSON Schema Format Standards
 * @link https://json-schema.org/
 * 
 * 
 */



// Required modules
const assert = require('assert');
const Ajv = require('ajv');
const { Then } = require('cucumber');



// Retrieving the schema information
const error_schema = require('../schemas/error.json');
const error_unauth_schema = require('../schemas/error_unauthorized.json');
const company_schema = require('../schemas/company.json');
const survey_template_schema = require('../schemas/survey_template.json');
const survey_template_list_schema = require('../schemas/survey_template_list.json');
const user_position_schema = require('../schemas/user_position_list.json');
const loan_folder_list_schema = require('../schemas/loan_folder_list.json');
const full_loan_folder_schema = require('../schemas/loan_folder_detailed.json');
const loan_folder_task_schema = require('../schemas/loan_folder_task.json');
const list_loan_folder_tasks_schema = {
    "type": "object",
    "properties": {
        "meta": {"type": "object"},
        "data": {
            "type": "array",
            "items": [
                loan_folder_task_schema
            ]
        }
    }
};
const lossystem_schema = require('../schemas/los_system.json');
const lossystem_documents_schema = require('../schemas/los_system_documents.json');
const lossystem_list_schema = {
    "type": "object",
    "properties": {
        "meta": {"type": "object"},
        "data": {
            "type": "array",
            "items": [
                lossystem_schema
            ]
        }
    }
};



// Schema Object
const schemas = {
    "error": error_schema,
    "unauth error": error_unauth_schema,
    "company object": company_schema,
    "survey template list": survey_template_list_schema,
    "survey template": survey_template_schema,
    "user position list": user_position_schema,
    "loan folder list": loan_folder_list_schema,
    "loan folder": full_loan_folder_schema,
    "task list": list_loan_folder_tasks_schema,
    "task": loan_folder_task_schema,
    "los list": lossystem_list_schema,
    "los documents": lossystem_documents_schema
};






Then('the response schema should be a/an {}', function(schema) {
    let ajv = new Ajv({allErrors: true});

    this.active_agents.forEach((agent) => {
        let valid = ajv.validate(schemas[schema], agent.get_response_body());

        if (!valid) {
            let message = `Account: ${agent.email} failed.\n`;
            ajv.errors.forEach((error) => {
                message += `${error.dataPath} ${error.message} but is actually: ${error.data}. \n`;
            });
            assert.fail(message);
        } else {
            assert(valid);
        }
    });
});

