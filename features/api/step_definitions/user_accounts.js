/**
 * Initialize Test Accounts Available.
 * 
 * Use this step to define accounts that will be used to execute the tests
 * 
 */


 
// Required modules
const axios = require('axios');
const { Given } = require('cucumber');
const Agent = require('./objects/agent');
const account_info = require('./objects/accounts.json');


/**
 * Adds access to test accounts and stores them into the corresponding array
 * @param {} email              email of test account
 * @param {} password           password of test account
 * @param {user_roles} role     role of test account
 * 
 * @example I have access to armando@streamloan.io with mortgage1 as loaner
 */
// Given('I have access to {} with {} as {}', function(email, password, role) {
//     let account = new Agent(this.web_url, email, password, role);

//     this.accounts.push(account);
// });



/**
 * Adds access to a public accounts which is one without authentication
 * 
 * @example I have access to a public account
 */
// Given('I have access to a public account', function() {
//     let public_account = new Agent(this.web_url, 'Public Account', 'No Password', 'Nothing');
//     public_account.access_token = 'InvalidToken';

//     this.accounts.push(public_account);
// });

Given('I am logged in as {word}', function(role) {
    if (!this.environment) return 'pending';
    let login_promises = [];

    if (role === 'nothing') {
        let new_agent = new Agent(this.web_url, 'public account', 'public password', 'none');
        this.active_agents.push(new_agent);

        login_promises.push(new_agent.login());
    } else {
        account_info[this.environment][role].forEach((account) => {
            let new_agent = new Agent(this.web_url, account.email, account.password, role);
            this.active_agents.push(new_agent);

            login_promises.push(new_agent.login());
        });
    }

    return Promise.allSettled(login_promises);
});