const { Then, Given } = require("cucumber");
const assert = require('assert');



Given('the access token is {word}', function(state) {
    switch (state) {
        case 'correct':
            break;
        
        case 'missing':
            this.active_agents.forEach((agent) => {
                agent.remove_query('access_token');
            });
            break;
        
        case 'invalid':
            this.active_agents.forEach((agent) => {
                agent.add_query_to('access_token', 'invalidtoken');
            });
            break;
    
        default:
            return 'pending'
    }
});

Given('the current lender position is {word}', function(state) {
    switch (state) {
        case 'missing':
            this.active_agents.forEach((agent) => {
                agent.remove_header('current-lender-position');
            });
            break;
        
        case 'invalid':
            this.active_agents.forEach((agent) => {
                agent.add_header_to('current-lender-position', '000');
            });
            break;
    
        default:
            return 'pending'
    }
});



Then('the error response message is {}', function(message) {
    this.active_agents.forEach((agent) => {
        let response_body = agent.get_response_body();

        let response_message = (response_body.error) ? response_body.error.message : response_body.message;
        
        assert.equal(message, response_message, `
        Response type failed for account ${agent.email}.\n
            Expected: ${message}.\n
            Actual: ${response_message}`)
    });
});