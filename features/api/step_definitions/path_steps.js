const { Given } = require("cucumber");


Given('I want to collect a company object', function() {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/public/companies');
    });
});

Given('I want to collect all survey templates', function () {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/private/surveytemplates');
    });
});

Given('I collect a survey template id', function () {
    if (this.active_agents[0].role === 'borrower') {
        this.active_agents.forEach((agent) => {
            agent.stored_data['survey_template_id'] = '4001';
        });
        return;
    }

    let response_promises = [];
    this.active_agents.forEach((agent) => {
        let fetch_response = agent.fetch('/api/private/surveytemplates', '0/templateId', 'survey_template_id');
        if (!fetch_response) return 'pending';

        response_promises.push(fetch_response);
    });

    return Promise.allSettled(response_promises);
});

Given('I want to collect more information for that survey template id', function () {
    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data('survey_template_id')) return 'pending';
    
        agent.add_path_to(`/api/private/surveytemplates/${agent.get_stored_data('survey_template_id')}`);
    });
});

Given('I want to collect all user positions', function() {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/private/user/positions');
    });
});

Given('I collect all my loan folders', function() {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/private/loanfolders');
    });
});

Given('I collect a loan folder id', function() {
    let response_promises = [];
    this.active_agents.forEach((agent) => {
        let fetch_response = agent.fetch('/api/private/loanfolders', '0/id', 'loan_folder_id');
        if (!fetch_response) return 'pending';

        response_promises.push(fetch_response);
    });

    return Promise.allSettled(response_promises);
});

Given('I want to collect more information for that loan folder id', function() {
    this.active_agents.forEach((agent) => {
        if(!agent.get_stored_data('loan_folder_id')) return 'pending';

        agent.add_path_to(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}`);
    });
});

Given('I want to update information for that loan folder id', function() {
    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data('loan_folder_id')) return 'pending';

        agent.add_path_to(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}`);
    });
});

Given('I want to collect all the tasks for a loan folder', function() {
    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data('loan_folder_id')) return 'pending';

        agent.add_path_to(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}/tasks`);
    });
});

Given('I collect a task id with that loan folder id', function() {
    let response_promises = [];
    this.active_agents.forEach((agent) => {
        let fetch_response = agent.fetch(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}/tasks`, '0/id', 'task_id');
        if (!fetch_response) return 'pending';

        response_promises.push(fetch_response);
    });

    return Promise.allSettled(response_promises);
});

Given('I want to collect more information for that task', function() {
    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data('loan_folder_id')) return 'pending';
        if (!agent.get_stored_data('task_id')) return 'pending';

        agent.add_path_to(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}/tasks/${agent.get_stored_data('task_id')}`);
    });
});

Given('I want to update information for that task id', function() {
    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data('loan_folder_id')) return 'pending';
        if (!agent.get_stored_data('task_id')) return 'pending';

        agent.add_path_to(`/api/private/loanfolders/${agent.get_stored_data('loan_folder_id')}/tasks/${agent.get_stored_data('task_id')}`);
    });
});

Given('I want to collect all los systems', function() {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/private/lossystems');
    });
});

Given('I want to collect all los documents related to the loan folder', function() {
    this.active_agents.forEach((agent) => {
        agent.add_path_to('/api/private/lossystems/documenttypes');
    });
});