// Required modules
const assert = require('assert');
const { Given, When, Then, setDefaultTimeout, Before } = require('cucumber');



// Give more time for api calls incase there is lag
setDefaultTimeout(60*1000);

// Initialize list of available accoutns for testing
Before(function() {
    this.active_agents = [];
});

Given('I set the {word} query to {}', function(key, value) {
    if (value === 'nothing') return;

    this.active_agents.forEach((agent) => {
        agent.add_query_to(key, value);
    });
});

Given('I set the {word} query from {}', function(key, stored_key) {
    if (stored_key === 'nothing') return;

    this.active_agents.forEach((agent) => {
        if (!agent.get_stored_data(stored_key)) return 'pending';

        agent.add_query_from(key, stored_key);
    });
});

Given('I store {} as/the {} in stored data', function(value, key) {
    if (value === key) return;
    let original_data = this.active_agents.map((agent) => {
        return agent.get_stored_data(key);
    });

    this.active_agents.forEach((agent, idx) => {
        if (value === 'from other agents') {
            agent.add_stored_data(key, original_data[((idx+1)%(this.active_agents.length))])
        } else {
            agent.add_stored_data(key, value);
        }
    });
});

Given('I set the body to {}', function(body) {
    if (body === null) return;

    let key_value_pair = body.split(';');
    this.active_agents.forEach((agent) => {
        key_value_pair.forEach((pair) => {
            let pair_data = pair.split('=');
            if (pair_data.length !== 2) return 'pending';

            agent.add_body_to(pair_data[0], pair_data[1]);
        });
    });
});

When('I send {word} request', function(method) {
    let response_promises = [];

    this.active_agents.forEach((agent) => {
        agent.set_method(method);
        response_promises.push(agent.send());
    });

    return Promise.allSettled(response_promises);
});

Then('the response status should be {}', function(status) {
    this.active_agents.forEach((agent) => {
        assert.equal(status, agent.get_status(), `
            Status failed for account ${agent.email}. \n
            Expected: ${status}.\n
            Actual: ${agent.get_status()}.
        `);
    })
});

Then('the response type should be JSON', function() {
    this.active_agents.forEach((agent) => {
        assert.equal('application/json', agent.get_response_type(), `
            Response type failed for account ${agent.email}.\n
            Expected: application/json.\n
            Actual: ${agent.get_response_type()}`);
    });
});

Then('the response body should have {}', function(body) {
    if (body === null) return assert(true);

    let key_value_pair = body.split(';');
    this.active_agents.forEach((agent) => {
        let response = agent.get_response_body().data;
        key_value_pair.forEach((pair) => {
            let pair_data = pair.split('=');
            let response_value = response[pair_data[0]];
            if (typeof response_value === "boolean") (response_value = response_value.toString());

            assert.equal(response_value, pair_data[1], `
            Response body failed for account ${agent.email}. \n
            Expected: ${pair_data[1]}\n
            Actual: ${response[pair_data[0]]}`);
        });
    });;
})