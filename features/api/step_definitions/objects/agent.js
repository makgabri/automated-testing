'use strict';

const axios = require('axios');
const Path = require('path');

const methods = ["GET", "POST", "DELETE", "PATCH"];


module.exports = class Agent {

    /**
     * 
     * @param {string} base_url     base url for main request/all request
     * @param {string} email        email of account being tested
     * @param {string} password     password of account being tested
     * @param {string} role         role of account
     * @param {string} body         body of main request
     * @param {string} header       header of main request
     * @param {string} query        query of main request
     * @param {string} stored_data  an object to store data
     * @param {string} invalid      states whether this account cannot be tested
     */
    constructor(base_url, email, password, role) {
        this.base_url = base_url;
        this.email = email;
        this.password = password;
        this.role = role;
        this.body = {};
        this.header = {};
        this.query = {};
        this.stored_data = {};
        this.invalid = false
    }

    send() {
        // Prepare url
        let url = this.base_url + this.path;

        // Prepare query
        let query_question_mark = false;
        for (const [key, value] of Object.entries(this.query)) {
            if (!query_question_mark) {
                url += `?${key}=${value}`;
                query_question_mark = true;
            } else {
                url += `&${key}=${value}`;
            }
        }

        // Send request
        let body = (this.method !== "GET") ? this.body : undefined;
        let request = (axios({
            method: this.method,
            url: url,
            headers: this.header,
            data: body
        }));
        // console.log(url, this.header, body);

        // Update response;
        request.then((response) => {
            this.response = response;
        }).catch((err) => {
            this.response = err.response;
        });

        // Return Promise
        return request;
    }



    /** Main Request Preparation **/

    /**
     * Log into account
     * 
     * @return {Promise} - the login promise to wait for completion
     */
    login() {
        if (this.role === 'none') return (new Promise((res, rej) => {res(true)}));

        let login_response = (axios.post(`${this.base_url}/restapi/user/login`, {
            email: this.email,
            password: this.password
        }));
        login_response.then((response) => {
            this.query['access_token'] = response.data.data.token;

            // Store current lender position if it exists
            if (response.data.data.user && response.data.data.user.position &&
                response.data.data.user.position[0] && response.data.data.user.position[0]["PK01"]) {
                this.header["current-lender-position"] = response.data.data.user.position[0]["PK01"]
            }
        }).catch((err) => {
            // Login failure -> Usually for the Public account since email and password are invalid
            console.log(`Failed to login for account with email: ${this.email}`);
        });
        return login_response;
    }

    /**
     * Sets method for main request
     * 
     * @param {string} method the method of main request
     * 
     * @return {boolean} - true if successful, false otherwise
     */
    set_method(method) {
        if (!methods.includes(method.toUpperCase())) return false;

        this.method = method;
        return true;
    }

    /**
     * Set body for main request
     * 
     * @param {string} key      key of data
     * @param {string} value    value of data
     */
    add_body_to(key, value) {
        this.body[key] = value;
    }
    
    /**
     * Set body for main request from fetched data
     * 
     * @param {string} key          key of data
     * @param {string} stored_key   key of stored data
     */
    add_body_from(key, stored_key) {
        if (!this.stored_data[stored_key]) return false;

        this.body[key] = this.stored_data[stored_key];
        return true;
    }

    /**
     * Set query for main request
     * 
     * @param {string} key      key of data
     * @param {string} value    value of data
     */
    add_query_to(key, value) {
        this.query[key] = value;
    }

    /**
     * Set query for main request from fetched data
     * 
     * @param {string} key          key of data 
     * @param {string} stored_key   key of stored data
     */
    add_query_from(key, stored_key) {
        if (!this.stored_data[stored_key]) return false;

        this.query[key] = this.stored_data[stored_key];
        return true;
    }
    
    /**
     * Remove a query with stored key
     * 
     * @param {string} key      key of data
     */
    remove_query(key) {
        if (!this.query[key]) return false;

        delete this.query[key];
        return true;
    }

    /**
     * Set header for main request
     * 
     * @param {string} key      key of data
     * @param {string} value    value of data
     */
    add_header_to(key, value) {
        this.header[key] = value;
    }
    
    /**
     * Set header for main request from fetched data
     * 
     * @param {string} key          key of data
     * @param {string} stored_key   key of stored data
     */
    add_header_from(key, stored_key) {
        if (!this.stored_data[stored_key]) return false;

        this.header[key] = this.stored_data[stored_key];
        return true;
    }

    /**
     * Remove a header with stored key
     * 
     * @param {string} key      key of data
     */
    remove_header(key) {
        if (!this.header[key]) return false;

        delete this.header[key];
        return true;
    }

    /**
     * Sets path for main request
     * 
     * @param {string} path     path of main request
     */
    add_path_to(path) {
        this.path = path;

        let path_array = path.split('/');
        this.api_public = (path_array[2] === "public") ? true : false;
    }

    /**
     * Update a path for main request
     * 
     * Automatically looks into path and see if there is a need to
     * replace any variables from those stored in stored_data
     * 
     * @param {string} var_in_path  variable in path to replace
     * @param {string} stored_key   key of stored data
     */
    update_path_from(var_in_path, stored_key) {
        if (!this.path) return false;
        if (!this.path.includes(var_in_path)) return false;

        this.path = this.path.replace(var_in_path, this.stored_data[stored_key]);
        return true;
    }

    /**
     * Update a path for main request
     * 
     * @param {string} var_in_path  variable in path to replace
     * @param {string} value        value of data
     */
    update_path_to(var_in_path, value) {
        if (!this.path) return false;
        if (!this.path.includes(var_in_path)) return false;

        this.path = this.path.replace(var_in_path, value);
        return true;
    }

    /**
     * Fetches data from another api
     * 
     * @param {string} path             path to get data
     * @param {string} prop_in_response  variable to look for data(loanfolder/lossystem)
     * @param {string} stored_key       key to store response and reference for future use
     * 
     * @return {boolean} -  false if access token was not retrieved(login to retrieve it)
     * @return {null}    -  null means that requested response was a list but list was empty
     * @return {Promise} -  the promise of the request
     */
    fetch(path, prop_in_response, stored_key) {
        //if (!this.stored_data.access_token) return false;
        if (!this.query['access_token']) return false;

        // Prepare url path & queries
        let path_list = path.split('/');
        let stored_data_key = Object.keys(this.stored_data);
        
        for (let i = 0; i < path_list.length; i++) {
            let curr = path_list[i];
            if (stored_data_key.includes(curr)) {
                path_list[i] = this.stored_data[curr];
            }
        }

        let url = new URL(Path.join(...path_list), this.base_url).href;

        // Prepare header & query 
        //let fetch_header = {}
        //if (this.stored_data["current-lender-position"]) (fetch_header["current-lender-position"] = this.stored_data["current-lender-position"]);

        url += `?access_token=${this.query['access_token']}`;
        for (const [key, value] of Object.entries(this.query)) {
            url += `&${key}=${value}`;
        }

        // Send Getter
        let request = (axios({
            method: "GET",
            url: url,
            headers: this.header
        }));

        // Update stored_data
        request.then((response) => {
            let target_data = response.data.data;
            let prop_in_response_list = prop_in_response.split('/');

            prop_in_response_list.forEach((prop) => {
                if (!target_data[prop]) return false;
                target_data = target_data[prop];
            });

            // Store data
            this.stored_data[stored_key] = target_data;
        }).catch((err) => {
            // Getting data returned false
        });

        // Return Promise
        return request;
    }



    /** Getters **/
    /**
     * @return {string} - role of this account
     */
    get_role() {
        return this.role;
    }

    get_stored_data(key) {
        if (!Object.keys(this.stored_data).includes(key)) return null;

        return this.stored_data[key];
    }

    add_stored_data(key, value) {
        this.stored_data[key] = value;
    }

    /**
     * @return {integer} - status of response
     */
    get_status() {
        return this.response.status;
    }

    /**
     * @return {object} - response body
     */
    get_response_body() {
        return this.response.data;
    }

    get_response_type() {
        return this.response.headers['content-type'];
    }

}