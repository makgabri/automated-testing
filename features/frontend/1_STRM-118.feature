Feature: Streamloan login page

	#Ensure the login page is as expected.
	@TEST_STRM-118
	Scenario: Streamloan login page
		Given I am on the https://master-webapp.streamloan.io/login page
		Then the webpage should be the same as login.png
