const { Given, When, Then, AfterAll } = require('cucumber');
const { Builder, By, Capabilities, Key } = require('selenium-webdriver');
const looksSame = require('looks-same');
const assert = require('assert');
let fs = require('fs');

require("chromedriver");

/**
 * Save screenshot from driver into an image
 * 
 * @param {string} data base64 encoded string of screen shot from driver
 * @param {string} name name to save image
 */
function writeScreenshot(data, name) {
    name = name || 'no_name.png';
    var screenshotPath = './features/frontend/temp/';
    fs.writeFileSync(screenshotPath + name, data, 'base64');
};

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });
const driver = new Builder().withCapabilities(capabilities).build();
driver.manage().setTimeouts( { implicit: 3000 } );

Given('I am on the {} page', async function (url) {
    await driver.get(url);
});

Then('the webpage should be the same as {}', async function (image) {
    // Give time to load website
    let webElement = driver.findElement(By.id("app"));
    return driver.takeScreenshot().then(function(data) {
        writeScreenshot(data, image);
        looksSame(`./features/frontend/images/${image}`, `./features/frontend/temp/${image}`, {ignoreCaret: true, tolerance: 5}, function(error, {equal}) {
            if (!equal) {
                return looksSame.createDiff({
                    reference: `./features/frontend/images/${image}`,
                    current: `./features/frontend/temp/${image}`,
                    diff: `./features/frontend/diffs/${image}`,
                    highlightColor: '#ff00ff', // color to highlight the differences
                    strict: false, // strict comparsion
                    tolerance: 2.5,
                    antialiasingTolerance: 0,
                    ignoreAntialiasing: true, // ignore antialising by default
                    ignoreCaret: true // ignore caret by default
                });
            }
            assert(equal);
        });
    });
});

AfterAll('end', async function(){
    await driver.quit();
});